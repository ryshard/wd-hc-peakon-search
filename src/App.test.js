import React from "react";
import { render, cleanup } from "@testing-library/react";
import App from "./App";

describe("App basics", () => {
  let getByTestId;

  beforeEach(() => {
    ({ getByTestId } = render(<App />));
  });

  afterEach(() => {
    cleanup();
  });

  test("renders header", () => {
    const headerElement = getByTestId("pageHeader");
    expect(headerElement).toBeInTheDocument();
  });

  test("renders pageContent", () => {
    const contentElement = getByTestId("pageContent");
    expect(contentElement).toBeInTheDocument();
  });

  test("renders pageFooter", () => {
    const footerElement = getByTestId("pageFooter");
    expect(footerElement).toBeInTheDocument();
  });
});
