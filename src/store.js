import thunk from "redux-thunk";
import { createStore, compose, applyMiddleware } from "redux";
import { devToolsEnhancer } from "redux-devtools-extension";
import DevTools from "./components/dev-tools";
import { initialState as initialAppState } from "./reducers/";
import rootReducer from "./reducers";

const filterMiddleware = () => {
  const args = [applyMiddleware(thunk)];
  if (window.__REDUX_DEVTOOLS_EXTENSION__) {
    args.push(devToolsEnhancer());
  } else {
    args.push(DevTools.instrument());
  }
  return args;
};

export const loadStore = ({ initialState = initialAppState } = {}) => {
  const composeArgs = filterMiddleware();
  return createStore(rootReducer, initialState, compose(...composeArgs));
};
