import React from "react";
import { Provider } from "react-redux";
import DevTools from "./components/dev-tools";
import { loadStore } from "./store";
import Home from "./containers/home";
import Layout from "./components/layout";
import "./App.css";

const store = loadStore();
const additionalDevTools = !window.__REDUX_DEVTOOLS_EXTENSION__;

function App() {
  return (
    <Provider store={store}>
      <Layout>
        <Home />
      </Layout>
      {additionalDevTools && <DevTools />}
    </Provider>
  );
}

export default App;
