import React from "react";
import { fireEvent } from "@testing-library/react";
import { peopleMock } from "../../utils/mocks";
import { renderWithRedux } from "../../utils/test";
import PeopleSearch from ".";
import { KEYS } from "../../constants";

const setup = (props = {}, state = {}) =>
  renderWithRedux(<PeopleSearch {...props} />, { initialState: { ...state } });

describe("PeopleSearch", () => {
  describe("render", () => {
    it("renders wrapper", () => {
      const { getByTestId } = setup();
      expect(getByTestId("peopleSearch")).toBeInTheDocument();
    });

    it("renders input", () => {
      const { getByTestId } = setup();
      expect(getByTestId("peopleSearchInput")).toBeInTheDocument();
    });

    it("renders clear button", () => {
      const { getByTestId } = setup();
      expect(getByTestId("clearBtn")).toBeInTheDocument();
    });

    it("NOT render peopleList by default", () => {
      const { queryByTestId } = setup();
      expect(queryByTestId("peopleList")).not.toBeInTheDocument();
    });
  });

  describe("interactions", () => {
    it("renders peopleList when input is in focus", () => {
      const { queryByTestId } = setup();
      fireEvent.focus(queryByTestId("peopleSearchInput"));
      expect(queryByTestId("peopleList")).toBeInTheDocument();
    });

    it("input change", () => {
      const { getByTestId } = setup();
      fireEvent.change(getByTestId("peopleSearchInput"), {
        target: { value: "Foo" },
      });
      expect(getByTestId("peopleSearchInput")).toHaveValue("Foo");
    });

    it("clear", () => {
      const { getByTestId } = setup();
      fireEvent.change(getByTestId("peopleSearchInput"), {
        target: { value: "Foo" },
      });
      expect(getByTestId("peopleSearchInput")).toHaveValue("Foo");
      fireEvent.click(getByTestId("clearBtn"));
      expect(getByTestId("peopleSearchInput")).toHaveValue("");
    });
  });

  describe("key bindings", () => {
    let input;
    let component;

    beforeEach(() => {
      component = setup(
        {},
        {
          people: {
            list: [...peopleMock],
          },
        }
      );

      input = component.getByTestId("peopleSearchInput");
    });

    it("highlights first item on arrow Down", () => {
      fireEvent.focus(input);
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      const items = component.container.querySelectorAll(".person");
      expect(items[0]).toHaveClass("active");
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      expect(items[1]).toHaveClass("active");
    });

    it("highlights first item on arrow Down", () => {
      fireEvent.focus(input);
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      const items = component.container.querySelectorAll(".person");
      expect(items[2]).toHaveClass("active");
      fireEvent.keyDown(input, { keyCode: KEYS.UP });
      expect(items[1]).toHaveClass("active");
      fireEvent.keyDown(input, { keyCode: KEYS.UP });
      expect(items[0]).toHaveClass("active");
    });

    it("selects field by ENTER", () => {
      fireEvent.focus(input);
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      const items = component.container.querySelectorAll(".person");
      expect(items[2]).toHaveClass("active");
      fireEvent.keyDown(input, { keyCode: KEYS.ENTER });
      expect(input).toHaveValue(items[2].textContent);
    });

    it("selects field by CLICK", () => {
      fireEvent.focus(input);
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      fireEvent.keyDown(input, { keyCode: KEYS.DOWN });
      const items = component.container.querySelectorAll(".person");
      expect(items[2]).toHaveClass("active");
      fireEvent.click(items[2]);
      expect(input).toHaveValue(items[2].textContent);
    });
  });
});
