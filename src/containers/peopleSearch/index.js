/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { path, pathOr } from "ramda";
import { connect } from "react-redux";
import PeopleList from "../../components/peopleList";
import { filterPeople } from "./utils";
import { KEYS } from "../../constants";

export const PeopleSearch = ({ people } = {}) => {
  const inputRef = useRef();
  const [searchResults, setSearchResults] = useState(people);
  const [searchOpen, setSearchOpen] = useState(false);
  const [searchText, setSearchText] = useState("");
  const [currentIndex, setIndex] = useState(-1);

  const selectPerson = (index = currentIndex) => {
    const matchingPerson = searchResults[index];
    const matchingName = path(["attributes", "name"], matchingPerson);
    if (matchingName) {
      setSearchText(matchingName);
      setSearchOpen(false);
      if (inputRef && inputRef.current) {
        inputRef.current.blur();
        handleBlur();
      }
    }
  };

  const handleClear = () => {
    setSearchText("");
  };

  const handleBlur = () => {
    window.setTimeout(() => setSearchOpen(false), 200);
  };

  const handleFocus = () => {
    setSearchOpen(true);
  };

  const handleChange = (e) => {
    setSearchText(e.target && e.target.value);
  };

  const handleKeyDown = (e) => {
    if (e.keyCode === KEYS.DOWN) {
      const newIndex = currentIndex + 1;
      if (newIndex < searchResults.length) {
        setIndex(currentIndex + 1);
      }
    }

    if (e.keyCode === KEYS.UP) {
      const newIndex = currentIndex - 1;
      if (newIndex >= 0) {
        setIndex(newIndex);
      }
    }

    if (e.keyCode === KEYS.ENTER && currentIndex >= 0 && searchOpen) {
      selectPerson(currentIndex);
    }
  };

  const handleSelect = (id) => {
    const matchingIndex = people.findIndex((p) => p.id === id);
    selectPerson(matchingIndex);
  };

  useEffect(() => {
    if (searchText.length === 0) {
      setSearchResults(people);
    } else {
      const filteredResults = filterPeople(people, searchText);
      setSearchResults(filteredResults);
    }

    setIndex(-1);
  }, [searchText]);

  console.log({ currentIndex });
  return (
    <div className="peopleSearchContainer" data-testid="peopleSearch">
      <input
        type="text"
        onFocus={handleFocus}
        onBlur={handleBlur}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        value={searchText}
        ref={inputRef}
        data-testid="peopleSearchInput"
      />
      <button onClick={handleClear} className="clearBtn" data-testid="clearBtn">
        Clear
      </button>
      {searchOpen && (
        <PeopleList
          people={searchResults}
          selected={currentIndex}
          onSelect={handleSelect}
        />
      )}
    </div>
  );
};

PeopleSearch.propTypes = {
  people: PropTypes.arrayOf(PropTypes.shape()),
};
const mapStateToProps = (state) => ({
  people: pathOr([], ["people", "list"], state),
});

export default connect(mapStateToProps)(PeopleSearch);
