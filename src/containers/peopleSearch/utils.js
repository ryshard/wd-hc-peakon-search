import { pathOr } from "ramda";

export const filterPeople = (people, text) =>
  people.filter((person) => {
    const name = pathOr("", ["attributes", "name"], person).toLocaleLowerCase();
    const fullName = name.replace(" ", "").toLocaleLowerCase();
    const searchPhrase = text.toLocaleLowerCase();
    return fullName.includes(searchPhrase) || name.includes(searchPhrase);
  });
