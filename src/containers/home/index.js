/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { loadPeopleAction } from "../../actions/action";
import { connect } from "react-redux";
import { ACTION_STATUS } from "../../constants";
import Loading from "../../components/loading";
import PeopleSearch from "../peopleSearch";

export const Home = ({ loadPeople, loadStatus } = {}) => {
  const [isLoading, setLoading] = useState(true);
  useEffect(() => {
    if (
      loadStatus === ACTION_STATUS.NOT_STARTED &&
      typeof loadPeople === "function"
    ) {
      loadPeople();
    }
  }, []);

  useEffect(() => {
    if (loadStatus === ACTION_STATUS.SUCCESS) {
      setLoading(false);
    }
  }, [loadStatus]);

  if (isLoading) {
    return <Loading label="loading people data..." />;
  }

  return (
    <div className="home" data-testid="homeContent">
      <PeopleSearch />
    </div>
  );
};

Home.propTypes = {
  loadPeople: PropTypes.func,
  loadStatus: PropTypes.number,
};

const mapStateToProps = (state) => ({
  loadStatus: state.people.loadStatus,
});

export default connect(mapStateToProps, {
  loadPeople: loadPeopleAction,
})(Home);
