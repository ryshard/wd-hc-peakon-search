import React from "react";
import { renderWithRedux } from "../../utils/test";
import { Home } from ".";
import { stub, assert } from "sinon";
import { ACTION_STATUS } from "../../constants";

const setup = (props = {}) => renderWithRedux(<Home {...props} />);

describe("Home", () => {
  it("renders loading by default", () => {
    const { getByTestId } = setup();
    expect(getByTestId("loadingComponent")).toBeInTheDocument();
  });

  it("loads data", () => {
    const loadSpy = stub();
    setup({ loadPeople: loadSpy, loadStatus: ACTION_STATUS.NOT_STARTED });
    assert.called(loadSpy);
  });
  it("renders loading by default", () => {
    const { getByTestId } = setup({ loadStatus: ACTION_STATUS.SUCCESS });
    expect(getByTestId("homeContent")).toBeInTheDocument();
  });
});
