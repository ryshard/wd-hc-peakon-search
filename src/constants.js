export const API_URL = "https://gist.githubusercontent.com/daviferreira";

export const API_PATHS = {
  people:
    "/41238222ac31fe36348544ee1d4a9a5e/raw/5dc996407f6c9a6630bfcec56eee22d4bc54b518/employees.json",
};

export const ACTION_TYPE = {
  PEOPLE_LOAD_START: "PEOPLE_LOAD_START",
  PEOPLE_LOAD_SUCCESS: "PEOPLE_LOAD_SUCCESS",
  PEOPLE_LOAD_ERROR: "PEOPLE_LOAD_ERROR",
};

export const ACTION_STATUS = {
  NOT_STARTED: -1,
  IN_PROGRESS: 0,
  SUCCESS: 1,
  ERROR: 2,
};

export const KEYS = {
  UP: 38,
  DOWN: 40,
  ENTER: 13,
};
