import { stub, assert } from "sinon";
import { loadPeopleRequest } from "./requests";
import { API_URL, API_PATHS } from "../constants";

describe("Requests::loadRequest", () => {
  it("returns response", function (done) {
    const sample = { foo: "bar" };
    loadPeopleRequest({
      fetchUrl: stub().resolves({ json: () => sample }),
    }).then((response) => {
      expect(response).toEqual(sample);
      done();
    });
  });

  it("calls fetch with correct url", function (done) {
    const sample = { foo: "bar" };
    const stubFetch = stub().resolves({ json: () => sample });
    loadPeopleRequest({
      fetchUrl: stubFetch,
    }).then(() => {
      const expectedUrl = API_URL + API_PATHS.people;
      assert.calledWith(stubFetch, expectedUrl);
      done();
    });
  });
});
