import { API_URL, API_PATHS } from "../constants";

export const loadPeopleRequest = ({ fetchUrl = fetch } = {}) => {
  let requestUrl = API_URL;
  requestUrl += API_PATHS.people;

  return fetchUrl(requestUrl).then((response) => response.json());
};
