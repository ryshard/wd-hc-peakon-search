import { ACTION_TYPE } from "../../constants";
import { loadPeopleRequest } from "../requests";

export const loadPeopleAction =
  ({ load = loadPeopleRequest } = {}) =>
  (dispatch) => {
    const onError = (e) => {
      dispatch({
        type: ACTION_TYPE.PEOPLE_LOAD_ERROR,
        error: e,
      });
      return new Error(e);
    };

    const onSuccess = (data) => {
      dispatch({
        type: ACTION_TYPE.PEOPLE_LOAD_SUCCESS,
        data,
      });
      return data;
    };

    dispatch({ type: ACTION_TYPE.PEOPLE_LOAD_START });

    return load().then(onSuccess).catch(onError);
  };
