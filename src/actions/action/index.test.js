import { ACTION_TYPE } from "../../constants";
import { stub, assert } from "sinon";
import { loadPeopleAction } from ".";

describe("Actions::loadPeople", () => {
  it("dispatches start", () => {
    const dispatchMock = stub();
    const mockRequest = stub().resolves([]);
    loadPeopleAction({ ratesRequest: mockRequest })(dispatchMock)
      .then(() => {
        assert.calledWith(dispatchMock, {
          type: ACTION_TYPE.PEOPLE_LOAD_START,
        });
      })
      .catch();
  });

  it("dispatches success", () => {
    const dispatchMock = stub();
    const mockRequest = stub().resolves([]);
    loadPeopleAction({ ratesRequest: mockRequest })(dispatchMock)
      .then(() => {
        assert.calledWith(dispatchMock, {
          type: ACTION_TYPE.PEOPLE_LOAD_SUCCESS,
          data: [],
        });
      })
      .catch(() => {
        assert.notCalled(dispatchMock);
      });
  });

  it("dispatches error if request fails", () => {
    const dispatchMock = stub();
    const sampleError = new Error("Boo");
    const mockRequest = stub().rejects(sampleError);
    loadPeopleAction({ ratesRequest: mockRequest })(dispatchMock)
      .then(() => {
        assert.notCalled(dispatchMock);
      })
      .catch(() => {
        assert.calledWith(dispatchMock, {
          type: ACTION_TYPE.PEOPLE_LOAD_ERROR,
          error: sampleError,
        });
      });
  });
});
