import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
    margin: "20px auto",
  },
}));

export const Loading = ({ label = "Loading..." } = {}) => {
  const classes = useStyles();

  return (
    <div className={classes.root} data-testid="loadingComponent">
      <Typography color="primary">{label}</Typography>
      <LinearProgress />
    </div>
  );
};

Loading.propTypes = {
  label: PropTypes.string,
};

export default Loading;
