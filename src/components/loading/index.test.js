import React from "react";
import { render } from "@testing-library/react";
import Loading from ".";

const setup = (props = {}) => render(<Loading {...props} />);

describe("Loading", () => {
  it("renders wrapper", () => {
    const { getByTestId } = setup();
    expect(getByTestId("loadingComponent")).toBeInTheDocument();
  });
});
