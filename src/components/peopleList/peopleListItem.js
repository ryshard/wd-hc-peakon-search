import React, { useRef, useEffect } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { path } from "ramda";

export const PeopleListItem = ({ person = {}, isSelected, onClick } = {}) => {
  const name = path(["attributes", "name"], person);
  const elemRef = useRef();

  useEffect(() => {
    if (isSelected && elemRef.current) {
      const el = elemRef.current;
      if (typeof el.scrollIntoView === "function") {
        el.scrollIntoView();
      }
    }
  }, [isSelected]);

  return (
    <div
      className={classNames("person", { active: isSelected })}
      data-testid={`person-${person.id}`}
      onClick={onClick}
      data-personid={person.id}
      ref={elemRef}
    >
      {name}
    </div>
  );
};

PeopleListItem.propTypes = {
  people: PropTypes.arrayOf(PropTypes.shape()),
  isSelected: PropTypes.bool,
  onClick: PropTypes.func,
};

export default PeopleListItem;
