import React from "react";
import { render, fireEvent } from "@testing-library/react";
import PeopleListItem from "./peopleListItem";
import { stub, assert } from "sinon";

const setup = (props = {}) =>
  render(<PeopleListItem person={{ id: "1234" }} {...props} />);

describe("PeopleListItem", () => {
  it("renders wrapper", () => {
    const { getByTestId } = setup();
    expect(getByTestId("person-1234")).toBeInTheDocument();
  });

  it("calls onClick", () => {
    const onClickSpy = stub();
    const { getByTestId } = setup({ onClick: onClickSpy });
    assert.notCalled(onClickSpy);
    fireEvent.click(getByTestId("person-1234"));
    assert.called(onClickSpy);
  });
});
