import React from "react";
import { path } from "ramda";
import PropTypes from "prop-types";
import classNames from "classnames";
import PeopleListItem from "./peopleListItem";

const ELEMENTS_IN_VIEW = 2;

export const PeopleList = ({ people = [], selected, onSelect } = {}) => {
  const handleItemClick = (e) =>
    onSelect(path(["target", "dataset", "personid"], e));

  const isScroll = people.length > 2;

  let showMore;
  let showPrev;

  if (isScroll) {
    showMore = selected < people.length - ELEMENTS_IN_VIEW;
    showPrev = selected > 0;
  }

  if (people.length === 0) {
    return (
      <div className="peopleList" data-testid="peopleList">
        <span data-testid="noResults">No results...</span>
      </div>
    );
  }
  return (
    <div className="peopleList" data-testid="peopleList">
      <div className={classNames("showMore", { hidden: !showPrev })}>
        <span>...</span>
      </div>
      <div
        className={classNames("peopleListContainer", { noScroll: !isScroll })}
        data-testid="peopleListContainer"
      >
        {people.map((person, i) => (
          <PeopleListItem
            person={person}
            key={person.id}
            isSelected={selected === i}
            onClick={handleItemClick}
          />
        ))}
      </div>
      <div className={classNames("showMore", { hidden: !showMore })}>
        <span>...</span>
      </div>
    </div>
  );
};

PeopleList.propTypes = {
  people: PropTypes.arrayOf(PropTypes.shape()),
  selected: PropTypes.number,
  onSelect: PropTypes.func,
};

export default PeopleList;
