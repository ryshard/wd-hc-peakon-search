import React from "react";
import { render } from "@testing-library/react";
import { peopleMock } from "../../utils/mocks";

import PeopleList from ".";

const setup = (props = {}) => render(<PeopleList {...props} />);

describe("PeopleList", () => {
  it("renders wrapper..", () => {
    const { getByTestId } = setup();
    expect(getByTestId("peopleList")).toBeInTheDocument();
  });

  it("renders peopleListContainer", () => {
    const { getByTestId } = setup({
      people: peopleMock,
    });
    expect(getByTestId("peopleListContainer")).toBeInTheDocument();
  });

  it("renders items", () => {
    const { container } = setup({
      people: peopleMock,
    });

    const items = container.querySelectorAll(".person");
    expect(items).toHaveLength(peopleMock.length);
  });

  it("renders selected", () => {
    const { container } = setup({
      people: peopleMock,
      selected: 1,
    });

    const items = container.querySelectorAll(".person");
    expect(items[0]).not.toHaveClass("active");
    expect(items[1]).toHaveClass("active");
  });
});
