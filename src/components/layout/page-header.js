import React from "react";
import PropTypes from "prop-types";
import Container from "@material-ui/core/Container";

export const PageHeader = ({ children, title } = {}) => (
  <header className="pageHeader" data-testid="pageHeader">
    <Container maxWidth="md">
      <h1>{title}</h1>
      {children}
    </Container>
  </header>
);

PageHeader.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
};
export default PageHeader;
