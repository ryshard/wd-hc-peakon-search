import React from "react";
import PropTypes from "prop-types";
import Container from "@material-ui/core/Container";
import PageHeader from "./page-header";
import PageFooter from "./page-footer";

export const Layout = ({ children } = {}) => (
  <div className="page">
    <PageHeader title="Peakon - managers search" />
    <main className="pageContent" data-testid="pageContent">
      <Container maxWidth="md">{children}</Container>
    </main>
    <PageFooter />
  </div>
);

Layout.propTypes = {
  children: PropTypes.node,
};

export default Layout;
