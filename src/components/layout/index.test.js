import React from "react";
import { render } from "@testing-library/react";
import Layout from ".";

const setup = (props = {}) =>
  render(
    <Layout {...props}>
      <div>Foo</div>
    </Layout>
  );

describe("Layout", () => {
  it("renders wrapper", () => {
    const { container } = setup();
    expect(container.querySelector(".page")).toBeInTheDocument();
  });

  it("renders header", () => {
    const { getByTestId } = setup();
    expect(getByTestId("pageHeader")).toBeInTheDocument();
  });

  it("renders footer", () => {
    const { getByTestId } = setup();
    expect(getByTestId("pageFooter")).toBeInTheDocument();
  });
});
