import React from "react";
import PropTypes from "prop-types";
import Container from "@material-ui/core/Container";
import Avatar from "@material-ui/core/Avatar";
import Chip from "@material-ui/core/Chip";
import myself from "../../assets/gg.jpg";

export const PageFooter = ({ children } = {}) => (
  <footer className="pageFooter" data-testid="pageFooter">
    <Container maxWidth="md">
      <Chip
        avatar={<Avatar alt="Gabriel Garus" src={myself} />}
        label="Coded by: Gabriel Garus"
      />
      {children}
    </Container>
  </footer>
);

PageFooter.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
};
export default PageFooter;
