import { ACTION_STATUS, ACTION_TYPE } from "../constants";
import peopleReducer, { initialState } from "./people";

describe("Reducer::people", () => {
  it("handles START", () => {
    const newState = peopleReducer(initialState, {
      type: ACTION_TYPE.PEOPLE_LOAD_START,
    });
    expect(newState).toMatchObject({
      ...initialState,
      loadStatus: ACTION_STATUS.IN_PROGRESS,
    });
  });

  it("handles SUCCESS", () => {
    const newState = peopleReducer(initialState, {
      type: ACTION_TYPE.PEOPLE_LOAD_SUCCESS,
    });
    expect(newState).toMatchObject({
      ...initialState,
      loadStatus: ACTION_STATUS.SUCCESS,
    });
  });

  it("handles ERROR", () => {
    const newState = peopleReducer(initialState, {
      type: ACTION_TYPE.PEOPLE_LOAD_ERROR,
    });
    expect(newState).toMatchObject({
      ...initialState,
      loadStatus: ACTION_STATUS.ERROR,
    });
  });

  it("returns unchanged state by default", () => {
    const newState = peopleReducer(initialState, {});
    expect(newState).toMatchObject(initialState);
  });
});
