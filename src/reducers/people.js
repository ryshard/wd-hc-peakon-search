import { pathOr } from "ramda";
import { ACTION_TYPE, ACTION_STATUS } from "../constants";

export const initialState = {
  loadStatus: ACTION_STATUS.NOT_STARTED,
  list: [],
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case ACTION_TYPE.PEOPLE_LOAD_START:
      return {
        ...state,
        loadStatus: ACTION_STATUS.IN_PROGRESS,
      };

    case ACTION_TYPE.PEOPLE_LOAD_SUCCESS:
      return {
        ...state,
        list: pathOr(state.list, ["data", "data"], action),
        loadStatus: ACTION_STATUS.SUCCESS,
      };

    case ACTION_TYPE.PEOPLE_LOAD_ERROR:
      return {
        ...state,

        loadStatus: ACTION_STATUS.ERROR,
      };

    default:
      return state;
  }
};
