import { combineReducers } from "redux";
import people, { initialState as initialRatesState } from "./people";

export const initialState = {
  people: initialRatesState,
};

export default combineReducers({ people });
