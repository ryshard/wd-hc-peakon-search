export const peopleMock = [
  {
    type: "employees",
    id: "323",
    links: {
      self: "http://localhost:3000/v1/employees/323",
    },
    attributes: {
      identifier: null,
      firstName: "Harriet",
      lastName: "McKinney",
      name: "Harriet McKinney",
      features: ["engagement"],
      avatar: null,
      employmentStart: "2016-01-31T00:00:00.000Z",
      external: false,
      "Last Year Bonus": 3767,
      "Business Unit": "Sales",
      "Commute Time": 34,
      Age: "1984-02-08",
      Department: "Customer Care",
      Gender: "Female",
      "Job Level": "Manager",
      "Local Office": "Kuala Lumpur",
      "% of target": 88,
      Region: "APAC",
      Salary: 76000,
      Tenure: "2014-05-31",
    },
    relationships: {
      company: {
        data: {
          type: "companies",
          id: "5",
        },
      },
      account: {
        data: {
          type: "accounts",
          id: "324",
        },
      },
      phones: {
        data: [],
      },
      Manager: {
        data: {
          type: "employees",
          id: "201",
        },
      },
    },
  },
  {
    type: "employees",
    id: "139",
    links: {
      self: "http://localhost:3000/v1/employees/139",
    },
    attributes: {
      identifier: null,
      firstName: "Harriet",
      lastName: "Banks",
      name: "Harriet Banks",
      features: ["engagement"],
      avatar: null,
      employmentStart: "2016-01-31T00:00:00.000Z",
      external: false,
      "Last Year Bonus": 215835,
      "Business Unit": "People & Operations",
      "Commute Time": 18,
      Age: "1984-04-19",
      Department: "Learning and Development",
      Gender: "Female",
      "Job Level": "Executive",
      "Local Office": "New York",
      "% of target": 12,
      Region: "AMER",
      Salary: 332000,
      Tenure: "2009-06-30",
    },
    relationships: {
      company: {
        data: {
          type: "companies",
          id: "5",
        },
      },
      account: {
        data: {
          type: "accounts",
          id: "140",
        },
      },
      phones: {
        data: [],
      },
    },
  },
  {
    type: "employees",
    id: "142",
    links: {
      self: "http://localhost:3000/v1/employees/142",
    },
    attributes: {
      identifier: null,
      firstName: "Mathilda",
      lastName: "Summers",
      name: "Mathilda Summers",
      features: ["engagement"],
      avatar: null,
      employmentStart: "2016-01-31T00:00:00.000Z",
      external: false,
      "Last Year Bonus": 95050,
      "Business Unit": "Marketing",
      "Commute Time": 131,
      Age: "1976-12-19",
      Department: "Media",
      Gender: "Female",
      "Job Level": "Executive",
      "Local Office": "London",
      "% of target": 166,
      Region: "EMEA",
      Salary: 248000,
      Tenure: "2000-05-15",
    },
    relationships: {
      company: {
        data: {
          type: "companies",
          id: "5",
        },
      },
      account: {
        data: {
          type: "accounts",
          id: "143",
        },
      },
      phones: {
        data: [],
      },
      Manager: {
        data: {
          type: "employees",
          id: "139",
        },
      },
    },
  },
  {
    type: "employees",
    id: "145",
    links: {
      self: "http://localhost:3000/v1/employees/145",
    },
    attributes: {
      identifier: null,
      firstName: "Marguerite",
      lastName: "Ryan",
      name: "Marguerite Ryan",
      features: ["engagement"],
      avatar: null,
      employmentStart: "2016-01-31T00:00:00.000Z",
      external: false,
      "Last Year Bonus": 81656,
      "Business Unit": "Sales",
      "Commute Time": 111,
      Age: "1991-09-14",
      Department: "Sales Development",
      Gender: "Female",
      "Job Level": "Executive",
      "Local Office": "Berlin",
      "% of target": 121,
      Region: "EMEA",
      Salary: 341000,
      Tenure: "2015-01-29",
    },
    relationships: {
      company: {
        data: {
          type: "companies",
          id: "5",
        },
      },
      account: {
        data: {
          type: "accounts",
          id: "146",
        },
      },
      phones: {
        data: [],
      },
      Manager: {
        data: {
          type: "employees",
          id: "139",
        },
      },
    },
  },
];
