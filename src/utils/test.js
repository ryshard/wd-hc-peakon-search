import React from "react";
import { Provider } from "react-redux";
import { render } from "@testing-library/react";
import { loadStore } from "../store";

export const renderWithRedux = (
  component,
  { initialState, store = loadStore({ initialState }) } = {}
) => {
  const ReduxWrapper = ({ children }) => {
    return <Provider store={store}>{children}</Provider>;
  };
  return render(component, { wrapper: ReduxWrapper });
};
