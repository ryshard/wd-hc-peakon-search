# PEAKON manager search component.

<br />

## The app for HackerRank code chellenge.

<hr />

author: Gabriel Garus <br />

demo: [https://ryshard.gitlab.io/wd-hc-peakon-search/](https://ryshard.gitlab.io/wd-hc-peakon-search/)

<hr />

### Installation

Before you run the app, you need to install all dependencies.

`npm install`

_App has been tested on node.js version: 12.2.0. <br />
In case of any problems with installation check your node version or run `nvm use`._

<hr />

### Starting the App

To start app on local server use:
`npm start`

This should start webpack server and open new tab in your browser.
[http://localhost:8000](http://localhost:8000).

The page will reload if you make edits.<br />

<hr />

### Testing

To run the unit test suite:
`npm test`

When updating tests it might be helpful to use:
`npm run test:watch`
_It will run recently updated tests (not commited) and will watch for changes in test files._

<hr />

### Lint

To check code quality use
`npm run lint`

<hr />

### Build

`npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

<hr />

<sup>
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
</sup>
